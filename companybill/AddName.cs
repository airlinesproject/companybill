﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace companybill
{
    public partial class AddProduct : Form
    {
        SqlConnection conn;
        
        public AddProduct()
        {
            InitializeComponent();
        }

        //add button
        private void button1_Click(object sender, EventArgs e)
        {
            insertProductName();
        }

        //back button
        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            
        }

        //insert function

        public void insertProductName()
        {
            try
            {
                string productName = textBox1.Text.ToUpper();
                string str_conn = "Data Source=PAWAN;Initial Catalog=companybill;Integrated Security=True";
                conn = new SqlConnection(str_conn);

                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }

                //sql query

                string insertQuery = "Insert into productName(pName) VALUES('" + productName + "')";
                SqlCommand cmd = new SqlCommand(insertQuery, conn);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Product Name added successfully");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            
        }
    }
}
