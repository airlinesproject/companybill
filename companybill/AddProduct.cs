﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace companybill
{
    public partial class AddCategoryName : Form
    {
        SqlConnection conn;
        SqlCommand cmd;
        public AddCategoryName()
        {
            InitializeComponent();
        }

        //add button
        private void button2_Click(object sender, EventArgs e)
        {
            insertCategoryName();
        }

        //back button
        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            
        }

        public void insertCategoryName()
        {
            try
            {
                string categoryName = textBox1.Text.ToUpper();
                string strcon = "Data Source=PAWAN;Initial Catalog=companybill;Integrated Security=True";
                conn = new SqlConnection(strcon);

                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }

                string insertQuery = "Insert into productCategory(pCategory) Values('" + categoryName + "')";
                cmd = new SqlCommand(insertQuery, conn);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Category inserted successfully");

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
    }
}
