﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace companybill
{
    class Bill_BLL
    {
        public string name { get; set; }
        public string payment { get; set; }
        public int price { get; set; }
        public int discount { get; set; }
        public int vat { get; set; }
        public int amount { get; set; }
        public string product_name { get; set; }
        public string model_no { get; set; }
    }
}
