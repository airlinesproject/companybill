﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace companybill.BLL
{
    class EdetailsBLL
    {
        public string pid { get; set; }
        public string pName{ get; set; }
        public string pCategory{ get; set; }
        public int qty { get; set; }
        public int rate { get; set; }
        public int total { get; set; }

        
    }
}
