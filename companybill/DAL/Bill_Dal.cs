﻿using companybill.BLL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace companybill.DAL
{
    class Bill_Dal
    {
        static string myconnectionmystring = "Data Source=pawan;Initial Catalog=companybill;Integrated Security=True";
        public bool Insert(Bill_BLL bill_bll_obj)
        {
            bool _isSuccess = false;

            //sqlconnection
            SqlConnection conn = new SqlConnection(myconnectionmystring);

            try
            {
                //insert query in variable sql
                string sql = "Insert into Bill(name, product, model, payment_method, amount) Values(@name, @product, @model, @payment_method, @amount)";

                //creating SqlCommand
                SqlCommand cmd = new SqlCommand(sql, conn);

                //adding parameters for passing values to the query
                cmd.Parameters.AddWithValue("@name", bill_bll_obj.name);
                cmd.Parameters.AddWithValue("@product", bill_bll_obj.product_name);
                cmd.Parameters.AddWithValue("@model", bill_bll_obj.model_no);
                cmd.Parameters.AddWithValue("@payment_method", bill_bll_obj.payment);
                cmd.Parameters.AddWithValue("@amount", bill_bll_obj.amount);

                //making an int variable to check whether the query is executed successfully or not
                int rows = cmd.ExecuteNonQuery();

                if (rows> 0)
                {
                    _isSuccess = true;
                }
                else
                {
                    _isSuccess = false;
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            finally
            {
                //closing the connection
                conn.Close();
            }

            return _isSuccess;
        }
    }
}
