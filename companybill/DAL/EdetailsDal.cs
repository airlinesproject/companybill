﻿using companybill.BLL;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace companybill.DAL
{
    class EdetailsDal
    {
        // create a static string to connect database
        static string myconectionstring = "Data Source=pawan;Initial Catalog=companybill;Integrated Security=True";

        #region Select Data from Database Table
        public DataTable Select()
        {
            //create an object to connect database
            SqlConnection conn = new SqlConnection(myconectionstring);

            //Create DataTable to hold Data from Database
            DataTable dt = new DataTable();

            try
            {
                //write sql query to get data from database

                string sql = "SELECT * FROM edetails";

                //creat sqlcommand to execute query
                //SqlCommand cmd = new SqlCommand(sql, conn);

                //create sql dataAdapter for holding data temporarily from database
                SqlDataAdapter adapter = new SqlDataAdapter(sql,conn);

                //open database connection
                conn.Open();

                //transfer data from SqlDataAdapter to DataTable
                adapter.Fill(dt);

            }
            catch (Exception ex)
            {
                //shows error message if error occur
                MessageBox.Show(ex.Message);
            }
            finally
            {
                //Close database Connection
                conn.Close();
            }

            return dt;
        }
        #endregion

        #region Insert query with boolean returntype method
        public bool Insert(EdetailsBLL e_bll_obj)
        {
            //create a boolean varaible and set it to false
            bool _isSuccess = false;

            //create the object of sql connection to create database
            SqlConnection conn = new SqlConnection(myconectionstring);

            try
            {
                //create the sql connection
                string sql = "INSERT INTO edetails(Name, Category, Rate, Qty, Total) VALUES(@name, @cate,@rate,@qty, @total)";

                //create the sqlcommand to pass query
                SqlCommand cmd = new SqlCommand(sql, conn);

                //create the parameter to pass the value
                cmd.Parameters.AddWithValue("@name", e_bll_obj.pName);
                cmd.Parameters.AddWithValue("@cate", e_bll_obj.pCategory);
                cmd.Parameters.AddWithValue("@rate", e_bll_obj.rate);
                cmd.Parameters.AddWithValue("@qty", e_bll_obj.qty);
                cmd.Parameters.AddWithValue("@total", e_bll_obj.total);

                //open connection
                conn.Open();

                //Create and integer value to hold the value after the query is executed
                int rows = cmd.ExecuteNonQuery();

                //the value of rows will be greater than 0 if the query is executed succesfully
                //Else it'll be 0

                if (rows > 0)
                {
                    //query executed successfully
                    _isSuccess = true;
                }
                else
                {
                    //Failed to executed query
                    _isSuccess = false;
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            finally
            {
                //closing database connection
                conn.Close();
            }


            return _isSuccess;
        }
        #endregion

        #region Update data in Database

        public bool Update(EdetailsBLL e_bll_obj)
        {
            //create a boolean variable and set its default value to false
            bool _isSuccess = false;

            //Create sqlconnection object
            SqlConnection conn = new SqlConnection(myconectionstring);

            try
            {
                //write a sql query in string variable
                string sql = "UPDATE edetails SET Name=@name,Category=@cate,Rate=@rate,Qty=@qty,Total=@total where id= @id ";

                //create sqlcommand to pass query
                SqlCommand cmd = new SqlCommand(sql, conn);

                //create the parameters to add data to the query
                cmd.Parameters.AddWithValue("@id", e_bll_obj.pid);
                cmd.Parameters.AddWithValue("@name", e_bll_obj.pName);
                cmd.Parameters.AddWithValue("@cate", e_bll_obj.pCategory);
                cmd.Parameters.AddWithValue("@rate", e_bll_obj.rate);
                cmd.Parameters.AddWithValue("@qty", e_bll_obj.qty);
                cmd.Parameters.AddWithValue("@total", e_bll_obj.total);

                //open database connection
                conn.Open();
                

                //creating and integer value to check whether the query is executed successfully or not
                int rows = cmd.ExecuteNonQuery();

                //the value of rows will be greater than 0 if the query is executed succesfully
                //Else it'll be 0
                if (rows > 0)
                {
                    //query executed successfully
                    _isSuccess = true;
                }
                else
                {
                    //Faild to execute query
                    _isSuccess = false;
                }


            }
            catch (Exception ex)
            {
                //shows error message
                MessageBox.Show(ex.Message);
            }
            finally
            {
                //close sqlconnection
                conn.Close();
            }

            return _isSuccess;
        }
        #endregion

        #region Delete the data
        public bool Delete(EdetailsBLL e_bll_obj)
        {
            //Create a boolean variable and set its default value to false
            bool _isSuccess = false;

            //create the sqlconnection object for database connection
            SqlConnection conn = new SqlConnection(myconectionstring);

            //try catch finally block

            try
            {
                //write a sql delete query in a string variable
                string sql = "Delete From edetails Where id = @id";

                //Create the Sqlcommand to pass the above query
                SqlCommand cmd = new SqlCommand(sql, conn);

                //creating the parameters to add the data to pass from query
                cmd.Parameters.AddWithValue("@id", e_bll_obj.pid);

                //open connection
                conn.Open();

                //create an integer value to hold the value after the query is executed
                int rows = cmd.ExecuteNonQuery();

                //if the query is executed with success the value of rows will be greater than 
                //else the value of the rows will be 0 (Zero)

                if (rows > 0)
                {
                    //Query Executed Successfully
                    _isSuccess = true;
                }
                else
                {
                    //Faild to execute query
                    _isSuccess = false;
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            finally
            {
                //close the sqlconnection
                conn.Close();
            }

            return _isSuccess;
        }
        #endregion

        #region newDelete Function

        public bool newDelete(EdetailsBLL e_bll_obj)
        {
            bool _success = false;

            //connection
            SqlConnection con = new SqlConnection(myconectionstring);

            //try catch finally
            try
            {
                //sql string of query delete
                string sql = "Delete from edetails where id = @id";

               
               

                //sqlcommand
                SqlCommand cmd = new SqlCommand(sql,con);

                //adding paramerters
                cmd.Parameters.AddWithValue("@id",e_bll_obj.pid);
                //opening connection
                con.Open();

                //creating the variable to check whether query is executed or not
                int rows = cmd.ExecuteNonQuery();

                //if statement to check
                if (rows > 0)
                {
                    _success = true;
                }
                else
                {
                    _success = false;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                
            }
            finally
            {
                //close connection
                con.Close();
            }

            return _success;
        }

        #endregion
    }
}