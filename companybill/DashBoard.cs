﻿using companybill.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace companybill
{
    public partial class DashBoard : Form
    {
        SqlConnection conn;
        SqlCommand cmd;
        public DashBoard()
        {
            InitializeComponent();
        }

        //home menu
        private void homeToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
	//git practice
        //menu to add product window from dash board.
        private void aDDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new Form2().Show();
        }

        private void nAMEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new AddProduct().Show();
        }

        private void cATEGORYToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new AddCategoryName().Show();

        }

        private void dETAILSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new ViewSalesDetails().Show();
        }


        
        
        private void DashBoard_Load(object sender, EventArgs e)
        {
            var today = DateTime.Now;
            var onlydate = today.Year + "/" + today.Month + "/" + today.Day;
            label11.Text = "Date :- " + onlydate.ToString();

            string str_conn = "Data Source=PAWAN;Initial Catalog=companybill;Integrated Security=True";
            conn = new SqlConnection(str_conn);
            conn.Open();
            // TODO: This line of code loads data into the 'companybillDataSet2.edetails' table. You can move, or remove it, as needed.
            this.edetailsTableAdapter.Fill(this.companybillDataSet2.edetails);

        }


        //search function
        internal void searchmethod(string sql)
        {
            

            SqlDataAdapter da1 = new SqlDataAdapter(sql, conn);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            dataGridView1.DataSource = dt1;
        }

        //searchbox
        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            string searchbox = textBox6.Text;
            string sql;
            if (searchbox == null)
            {
                sql = "SELECT Id,Name,Category,Qty FROM `edetails`";
            }
            else
            {
                sql = "SELECT Id,Name,Category,Qty FROM edetails WHERE Name LIKE '" + searchbox + "%' OR Id LIKE '" + searchbox + "%'";
            }

            searchmethod(sql);

        }

        //crud operation
        internal void CRUD(string sql)
        {
            cmd = new SqlCommand(sql, conn);
            cmd.ExecuteNonQuery();
        }
        
        //double click event on datagridview.
        private void dataGridView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            string id = dataGridView1.SelectedCells[0].Value.ToString();
            //MessageBox.Show(id);

            string sql = "SELECT * FROM  edetails Where id='" + id + "' ";
            CRUD(sql);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {


                
                string categorylbl = reader["Category"].ToString(); //getting category in string ()
                label10.Text = reader["Name"].ToString() + "'s " + categorylbl; //setting label as heading. eg:- Dell's Mobile
                



            }
            reader.Close();
        }

        private void DashBoard_Activated(object sender, EventArgs e)
        {
            this.edetailsTableAdapter.Fill(this.companybillDataSet2.edetails);
        }

        //clearing function
        #region Clearing Function
        public void Clearing()
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            comboBox1.SelectedIndex = 0;
            textBox4.Clear();
            textBox5.Clear();
            textBox6.Clear();
            textBox7.Clear();
        }
        #endregion
        //Button click event = Issue.
        #region Issuing the bill to the customer Button
        private void button1_Click(object sender, EventArgs e)
        {
            Bill_BLL billobj = new Bill_BLL();
            Bill_DAL2 billdalobj = new Bill_DAL2();
            //bill => SN. , Product name , quantity, rate, amount , discount amount, vat amt, Final price
            billobj.name = textBox1.Text; //db
            billobj.payment = comboBox1.Text; //db
            billobj.price = Int32.Parse(textBox3.Text.ToString());
            billobj.discount = Int32.Parse(textBox4.Text.ToString());
            billobj.vat = Int32.Parse(textBox2.Text.ToString());
            billobj.amount = Int32.Parse(textBox5.Text.ToString()); //db
            billobj.product_name = label10.Text;//db
            billobj.model_no = textBox7.Text; //db

            
            

            //checking whether the query is executed successfully or not
            bool _isSuccess = billdalobj.Insert(billobj);
            if (_isSuccess == true)
            {
                MessageBox.Show("Bill issued");
            }
            else
            {
                MessageBox.Show("Unable to issue bill");
            }
            Clearing();
            
        }
        #endregion

        #region Initial price region => Event Handling on textchange

        
        private void textBox3_TextChanged(object sender, EventArgs e)
        {

            float price;
            //checking null
            if (textBox3.Text.ToString() == "")
            {
                price = 0;
            }
            else
            {
                
                price = Convert.ToSingle(textBox3.Text.ToString()); //price to int
            }
            float vat_amount = Convert.ToSingle(price * 0.13); //vat amount
            textBox2.Text = vat_amount.ToString(); //setting vat amount to textbox
            textBox5.Text = (price + vat_amount).ToString(); //total amount  textbox


        }
        #endregion

        #region Discount percentage calculation
        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            float price;
            float discount_per;
            if (textBox3.Text == "" || textBox4.Text == "")
            {
                price = 0;
                discount_per = 0;
            }
            else
            {
                price = Convert.ToSingle(Int32.Parse(textBox3.Text.ToString()));
               
                 discount_per = Convert.ToSingle(textBox4.Text.ToString());


                //string total_amount = (price * (discount_per / 100)).ToString();
                
                

            }
            float vat_amount = Convert.ToSingle(price * 0.13); //vat amount
            float total_amount = price - (price * (discount_per / 100)) + vat_amount;
            textBox5.Text = total_amount.ToString();
        }

        #endregion

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}

