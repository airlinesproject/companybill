﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using companybill.BLL;
using companybill.DAL;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace companybill
{


    public partial class Form2 : Form
    {
        #region creating Dll and Bll  class object

        
        //creating object of DLL and BLL
        EdetailsBLL e_bll_obj = new EdetailsBLL();
        EdetailsDal e_dal_obj = new EdetailsDal();
        #endregion

        //create global sqlconnection obj
        //create global sqlcommand obj
        SqlConnection conn;
        SqlCommand cmd;

        public Form2()
        {
            InitializeComponent();
        }
        //program executed after loading form.
        #region After loading the form
        private void Form2_Load(object sender, EventArgs e)
        {

            connection();

            combo(comboBox1, "Select * from productName", "pName");
            combo(comboBox2, "Select * from productCategory", "pCategory");
            //loaddata();
            loadgridview();
        }
        #endregion

        //connection of sql database method
        #region Connection function
        internal void connection()
        {

            string str_conn = "Data Source=PAWAN;Initial Catalog=companybill;Integrated Security=True";
            conn = new SqlConnection(str_conn);
            conn.Open();
        }
        #endregion

        //OLD load data 
        #region Loading data to DataGridView Function
        /*void loaddata()
        {
            string selectquery = "SELECT * FROM edetails";
            SqlDataAdapter da = new SqlDataAdapter(selectquery, conn);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }*/
        #endregion

        //NEW inserting to the datagridview using ::::::::::: DAL :::::::::: method
        #region loadgridview using Data Access Layer
        public void loadgridview()
        {
            dataGridView1.DataSource = e_dal_obj.Select();
        }
        #endregion
        //search
        #region Searching data from textbox function

        internal void searchmethod(string sql)
        {
            
            SqlDataAdapter da1 = new SqlDataAdapter(sql, conn);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            dataGridView1.DataSource = dt1;
        }
        #endregion

        //function to load data in combobox
        #region ComboBox Data from Database
        
        internal void combo(ComboBox comboObject,string sql, string colName)
        {
            try
            {
                cmd = new SqlCommand(sql, conn);
                cmd.ExecuteNonQuery();

                //dataadapter
                SqlDataAdapter da1 = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);

                foreach (DataRow dr in dt1.Rows)
                {
                    comboObject.Items.Add(dr[colName].ToString());
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            
        }
        #endregion

        //Create, Retrive, Update, Delete operation is executed after the sql string is loaded in CRUD( <<-- Here -->>) 
        #region CRUD Function
        internal void CRUD(string sql)
        {
            cmd = new SqlCommand(sql, conn);
            cmd.ExecuteNonQuery();
        }
        #endregion

        //clearing text field and comboBox.
        #region clear function
        void Clearing()
        {
            
            comboBox2.SelectedIndex = 0;
            comboBox1.SelectedIndex = 0;
            textBox2.Clear();
            textBox3.Clear();
        }
        #endregion

        #region Update function 
        void finalupdate(string id)
        {
            string name = comboBox1.SelectedItem.ToString();
            string cate = comboBox2.SelectedItem.ToString();
            int rate = Convert.ToInt32(textBox2.Text);
            int qty = Convert.ToInt32(textBox3.Text);
            int total = rate * qty;

            String updatesql = "UPDATE edetails SET Name='" + name + "',Category='" + cate + "',Rate='" + rate + "',Qty='" + qty + "',Total='"+total+"' where id='" + id + "'";
            CRUD(updatesql);
            loadgridview();
            Clearing();
            
        }
        #endregion

        //deleting from datagridview
        #region Delete Function
        void delete()
        {
            string id = dataGridView1.SelectedCells[0].Value.ToString(); //selects id from dataviewgrid
            String sql = "DELETE FROM edetails WHERE id ='" + id + "'";
            var selectedOption = MessageBox.Show("Do you want to Delete this data ? ", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (selectedOption == DialogResult.Yes)
            {
                CRUD(sql);
                loadgridview();
            }
        }
        #endregion

        //New code
        //function to store the value to the class property
        #region setting value in BLL 
        public void setPropertyValue()
        {
            e_bll_obj.pName = comboBox1.SelectedItem.ToString();
            e_bll_obj.pCategory = comboBox2.SelectedItem.ToString();
            e_bll_obj.rate = Convert.ToInt32(textBox2.Text);
            e_bll_obj.qty = Convert.ToInt32(textBox3.Text);
            e_bll_obj.total = e_bll_obj.rate * e_bll_obj.qty;
        }
        #endregion

        /*::::::::::::ALL:::::::::::::::
         :::::::::::EVENTS::::::::::::::
         ::::::::::BELOW::::::::::::::::*/


        //Insert = button1_click
        #region Insert Button Click Event Function
        private void button1_Click(object sender, EventArgs e)
        {
            //old code

            //if null halna baki xa.
            /*string name = comboBox1.SelectedItem.ToString();
            string cate = comboBox2.SelectedItem.ToString();
            
            int rate = Convert.ToInt32(textBox2.Text);
            int qty = Convert.ToInt32(textBox3.Text);
            int total = rate * qty;

*/

            //New code...
            setPropertyValue();


            //checking whether inserted or not
            bool success = e_dal_obj.Insert(e_bll_obj);
            if (success==true)
            {
                MessageBox.Show("inserted successfully");
            }
            else
            {
                MessageBox.Show("Failed to insert");
            }
            Clearing();
            loadgridview();
            /* string sql = "INSERT INTO edetails (Name,Category,Rate,Qty,Total) VALUES('" + name + "','" + cate + "','" + rate + "','" + qty + "','" + total + "')";
            CRUD(sql);
            loadgridview();
            Clearing();*/

        }
        #endregion

        //update = button3_Click
        #region Update Button Click Event Function
        private void button3_Click(object sender, EventArgs e)
        {
            e_bll_obj.pid = dataGridView1.SelectedCells[0].Value.ToString(); //update data from dataviewgrid

            //new code
            setPropertyValue();
            //old code
            var selectedOption = MessageBox.Show("Do you want to Update this data ? ", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (selectedOption == DialogResult.Yes)
            {
               // finalupdate(id);

                //New code
                

                //create a boolean variable to check the execution of query
                bool success = e_dal_obj.Update(e_bll_obj);
                loadgridview();
                if (success == true)
                {
                    
                    MessageBox.Show("Updated successfully");
                }
                else
                {
                    MessageBox.Show("Failed to Update");
                }
                
                Clearing();

            }
            
            
            
        }
        #endregion

        //delete = button2_Click
        #region Delete Button Click Event Function
        private void button2_Click(object sender, EventArgs e)
        {
            e_bll_obj.pid = dataGridView1.SelectedCells[0].Value.ToString();; //selects id from dataviewgrid
            
            var selectedOption = MessageBox.Show("Do you want to Delete this data ? ", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (selectedOption == DialogResult.Yes)
            {
                
                loadgridview();

                //creating the boolean variable to check the execution of the query
                bool _isSuccess = e_dal_obj.newDelete(e_bll_obj);

                // if e_dal_obj.Delete(Edetails object) returns true the operation is performed else failed
                if (_isSuccess == true)
                {
                    
                    MessageBox.Show("true");
                }
                else
                {
                    MessageBox.Show("false");
                }
            }
            
        }
        #endregion

        //search text change Event
        #region TextChange Event for Searching Data

        private void search_TextChanged(object sender, EventArgs e)
        {
            
            string searchbox = search.Text;
            string sql;
            if (searchbox == null) 
            {
                 sql = "SELECT * FROM `edetails`";
            }
            else
            {
                sql = "SELECT * FROM edetails WHERE Name LIKE '" + searchbox + "%' OR Id LIKE '" + searchbox + "%'";
            }
            
            searchmethod(sql);
            

        }
        #endregion

        //event on single click of mouse over the data
        #region dataGridView1_CellMouseClick Event
        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string id = dataGridView1.SelectedCells[0].Value.ToString();
            //MessageBox.Show(id);

            string sql = "SELECT * FROM  edetails Where id='" + id + "' ";
            CRUD(sql);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {


                comboBox1.Text = reader["Name"].ToString();
                comboBox2.Text = reader["Category"].ToString();
                textBox2.Text = reader["Rate"].ToString();
                textBox3.Text = reader["Qty"].ToString();



            }
            //always close the adapter after on iteration so that it can be again initiate for second iteration
            reader.Close();

        }
        #endregion

    }
}