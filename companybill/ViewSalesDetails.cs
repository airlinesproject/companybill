﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace companybill
{
    public partial class ViewSalesDetails : Form
    {
        public static string Connection_string = "Data Source=pawan;Initial Catalog=companybill;Integrated Security=True";
        SqlConnection conn = new SqlConnection(Connection_string);
        public ViewSalesDetails()
        {
            InitializeComponent();
        }

        private void ViewSalesDetails_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'customerdetails.Bill' table. You can move, or remove it, as needed.
            this.billTableAdapter1.Fill(this.customerdetails.Bill);
            // TODO: This line of code loads data into the 'billDataSet.Bill' table. You can move, or remove it, as needed.
            this.billTableAdapter.Fill(this.billDataSet.Bill);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }
        internal void searchmethod(string sql)
        {
            ;
            conn.Open();
            SqlDataAdapter da1 = new SqlDataAdapter(sql, conn);
            conn.Close();
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            dataGridView1.DataSource = dt1;
            
        }

        #region SearchBox TextChange Event
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string text = textBox1.Text.ToString();
            string sql;
            if (text == null)
            {
                sql = "SELECT * FROM Bill";
            }
            else
            {
                sql = "Select * from Bill Where Bill_no Like '" + text + "%' OR name Like '" + text + "%' ";
            }
            searchmethod(sql);

        }
        #endregion
        public string bill_no;
        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            bill_no= dataGridView1.SelectedCells[0].Value.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            conn.Open();
            string sql = "Delete From Bill where Bill_no = '"+bill_no+"'";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.ExecuteNonQuery();

            this.billTableAdapter1.Fill(this.customerdetails.Bill);
            
            this.billTableAdapter.Fill(this.billDataSet.Bill);

            conn.Close();
        }
    }
}
